package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func createMenu(strings []string) tgbotapi.ReplyKeyboardMarkup {
	var buttons = make([][]tgbotapi.KeyboardButton, len(strings))
	for i, s := range strings {
		buttons[i] = append(buttons[i], tgbotapi.NewKeyboardButton(s))
	}
	return tgbotapi.ReplyKeyboardMarkup{ResizeKeyboard: true, Keyboard: buttons}
}

func GetMenu() tgbotapi.ReplyKeyboardMarkup {
	return createMenu([]string{FAQButton, AboutButton})
}

func createInlineButtonsLinks(name string, link string) *tgbotapi.InlineKeyboardMarkup {
	var buttons []tgbotapi.InlineKeyboardButton
	buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonURL(name, link))
	mrk := tgbotapi.NewInlineKeyboardMarkup(buttons)
	return &mrk
}

func CreateFAQMessage(chatId int64) *tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(chatId, FAQText)
	msg.ReplyMarkup = createInlineButtonsLinks(FAQRefText, FAQRefLink)
	return &msg
}
