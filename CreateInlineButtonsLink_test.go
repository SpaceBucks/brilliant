package main

import "testing"

func TestCreateInlineButtonsLink(t *testing.T) {
	mrk := createInlineButtonsLinks("name", "link")
	if *mrk.InlineKeyboard[0][0].URL != "link" {
		t.Errorf("FAQ button link was incorrect, got: %s, want: %s.", *mrk.InlineKeyboard[0][0].URL, "link")
	}
}
