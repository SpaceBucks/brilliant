package main

import (
	"testing"
	"math/rand"
)

func TestFAQMessageChatId(t *testing.T) {
	chatId := rand.Int63()
	total := CreateFAQMessage(chatId)
	if total.ChatID != chatId {
		t.Errorf("FAQ chat id was incorrect, got: %d, want: %d.", total.ChatID, chatId)
	}
}
