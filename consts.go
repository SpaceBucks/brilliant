package main

const (
	AboutButton = "💁🏼‍♂О создателе"
	AboutText   = "Coming soon 🚀"
	WelcomeText = "Привет, меня зовут Леонид и я приветствую Вас в своём саппортом боте! 🤝\n" +
		"Пожалуйста, посмотрите информацию, доступную по кнопке FAQ❓\n" +
		"По всем вопросам просто пишите боту — мы ответим Вам в ближайшее время."
	FAQButton  = "❓FAQ"
	FAQText    = "Здесь Вы можете найти описания, инструкции и стоимости по моим проектам"
	FAQRefText = "Реферальный бот"
	FAQRefLink = "http://telegra.ph/Instrukciya-po-referalnomu-botu-04-03"
)

const (
	TOKEN           = ""
	SUPPORT_CHAT_ID = 0
)
