# Brilliant 

This project is a support bot for Telegram. When a user writes to the bot, his message will be forwarded to the chat. Anyone in the chat can reply to the message. The answer will be sent to the user. As a result, we get anonymity and any number of support staff. In addition, the bot contains a FAQ.
## Examples

You can try the instance of the bot [here](https://t.me/leonidsupportbot)
## Build

[Go SDK](https://golang.org/doc/install) is required to build the project. Put in console next commands:
```
go get
go build
```
## Test

To run test ou in console next command:
```
go test
```
