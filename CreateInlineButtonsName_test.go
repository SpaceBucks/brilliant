package main

import "testing"

func TestCreateInlineButtonsName(t *testing.T) {
	mrk := createInlineButtonsLinks("name", "link")
	if mrk.InlineKeyboard[0][0].Text != "name" {
		t.Errorf("FAQ button name was incorrect, got: %s, want: %s.", mrk.InlineKeyboard[0][0].Text, "name")
	}
}