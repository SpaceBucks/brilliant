package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/logmatic/logmatic-go"
	log "github.com/sirupsen/logrus"
)

var bot *tgbotapi.BotAPI
var err error

func main() {
	log.SetFormatter(&logmatic.JSONFormatter{})
	log.SetLevel(log.DebugLevel)

	bot, err = tgbotapi.NewBotAPI(TOKEN)
	if err != nil {
		log.Panic(err)
	}
	log.WithFields(log.Fields{"tag": "system"}).Debugf("Authorized on account %s", bot.Self.UserName)

	var updates tgbotapi.UpdatesChannel
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err = bot.GetUpdatesChan(u)

	if err != nil {
		log.Println(err)
	}

	for update := range updates {
		log.WithFields(log.Fields{"tag": "user",
			"username": update.Message.From.UserName,
			"chatID":   update.Message.Chat.ID}).Debug(update.Message.Text)

		if update.Message.Chat.ID == SUPPORT_CHAT_ID {
			dispatchSupport(update)
		} else {
			dispatcher(update)
		}
	}

}

func dispatchSupport(update tgbotapi.Update) {
	replyTo := update.Message.ReplyToMessage
	if replyTo == nil {
		return
	}
	chatId := int64(replyTo.ForwardFrom.ID)
	// https://github.com/go-telegram-bot-api/telegram-bot-api/issues/161
	msgId := replyTo.ForwardFromMessageID
	var msg tgbotapi.Chattable

	if update.Message.Sticker != nil {
		sticker := tgbotapi.NewStickerShare(chatId, update.Message.Sticker.FileID)
		sticker.ReplyToMessageID = msgId
		msg = sticker
	} else if update.Message.Photo != nil {
		photo := tgbotapi.NewPhotoShare(chatId, (*update.Message.Photo)[0].FileID)
		photo.Caption = update.Message.Caption
		photo.ReplyToMessageID = msgId
		msg = photo
	} else {
		msgSimple := tgbotapi.NewMessage(chatId, update.Message.Text)
		msgSimple.ReplyToMessageID = msgId
		msg = msgSimple
	}

	go bot.Send(msg)
}

func dispatcher(update tgbotapi.Update) {
	text := update.Message.Text
	chatId := update.Message.Chat.ID
	switch text {
	case "/start":
		sendMenu(chatId)
	case AboutButton:
		bot.Send(tgbotapi.NewMessage(chatId, AboutText))
	case FAQButton:
		bot.Send(CreateFAQMessage(chatId))
	default:
		go sendToSupport(update)
	}
}

func sendToSupport(update tgbotapi.Update) {
	bot.Send(tgbotapi.NewForward(SUPPORT_CHAT_ID, update.Message.Chat.ID, update.Message.MessageID))
}

func sendMenu(id int64) {
	msg := tgbotapi.NewMessage(id, WelcomeText)
	msg.ReplyMarkup = GetMenu()
	bot.Send(msg)
}
