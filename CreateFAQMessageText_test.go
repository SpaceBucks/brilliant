package main

import "testing"

func TestFAQMessageText(t *testing.T) {
	total := CreateFAQMessage(1)
	if total.Text != FAQText {
		t.Errorf("FAQ message was incorrect, got: %s, want: %s.", total.Text, FAQText)
	}
}

